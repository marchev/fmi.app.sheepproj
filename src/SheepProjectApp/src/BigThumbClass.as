package 
{
    import mx.controls.sliderClasses.SliderThumb;

    public class BigThumbClass extends SliderThumb
   {
        public function BigThumbClass() 
        {
            super();
            this.width = 100;
            this.height = 100;
        }
    }
}