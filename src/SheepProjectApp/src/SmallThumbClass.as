package 
{
    import mx.controls.sliderClasses.SliderThumb;

    public class SmallThumbClass extends SliderThumb
   {
        public function SmallThumbClass() 
        {
            super();
            this.width = 70;
            this.height = 70;
        }
    }
}