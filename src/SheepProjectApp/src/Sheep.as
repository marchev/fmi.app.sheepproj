package
{
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import mx.binding.utils.*;
	import mx.controls.HSlider;
	import mx.events.SliderEvent;
	
	[Bindable]
	public class Sheep extends HSlider
	{
		public var taskId:uint;
		public var taskOrdr:uint;
		public var taskName:String;
		public var taskDescription:String;
		public var taskColor:uint;
		public var taskPriority:int;
		public var taskCompletedDate:String;
		public var taskIsRemoved:String;
		
		[Embed(source="../assets/first.png")]
		public var FImg:Class;
		
		[Embed(source="../assets/second.png")]
		public var SImg:Class;
		
		[Embed(source="../assets/third.png")]
		public var TImg:Class;

		private var b:Boolean = true;
		
		public function Sheep(argId:uint, argOrdr:uint, argName:String, argDescr:String, argPrior:int, 
			argColor:uint, argProgress:Number, argCompletedDate:String, argIsRemoved:String)
		{
			super();
			
			taskId = argId;
			taskOrdr = argOrdr;
			taskName = argName;
			taskDescription = argDescr;
			taskPriority = argPrior;
			taskColor = argColor;
			taskCompletedDate = argCompletedDate;
			taskIsRemoved = argIsRemoved;
			value = argProgress;
			
			setStyle("thumbSkin", FImg);
			addEventListener(SliderEvent.THUMB_DRAG, moveSheep);
			//choose Sheep size depending on priority
			if( taskPriority == 0 )
			{
				sliderThumbClass = SmallThumbClass;
			}
			if( taskPriority == 1 )
			{
				sliderThumbClass = MiddleThumbClass;
			}
			if( taskPriority == 2 )
			{
				sliderThumbClass = BigThumbClass;
			}
			snapInterval = 0;
			liveDragging = true;
			showDataTip = false;
			tickInterval = 10;
			maximum = 100;
			minimum = 0;
			height = 100;
			percentWidth = 100;
			//toolTip = taskName;	// TODO
			setStyle("borderColor", taskColor);
			setStyle("trackColors", [taskColor, taskColor]);
		}
		
		public function moveSheep(event:Event):void
		{
			b = true;
			var t:Timer = new Timer(100, 1);
			t.addEventListener(TimerEvent.TIMER, timerHandler);
			t.start();
		}
			
		public function timerHandler(e:TimerEvent):void
		{
			if(getStyle("thumbSkin") == FImg && b)
			{
				setStyle("thumbSkin", SImg);
				b = !b;
			}
			else if(getStyle("thumbSkin") == FImg)
			{
				setStyle("thumbSkin", TImg);
				b = !b;
			}
			else
			{
				setStyle("thumbSkin", FImg);
			}
		}		
	}
}