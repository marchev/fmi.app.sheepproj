package
{
   import mx.controls.ColorPicker;
 
   public class CustomColorPicker extends ColorPicker
   {
      [Bindable]
      public var niceColors:Array = [
      			{label:"White", color:"0xF2F2F2"},
      			{label:"Green 1", color:"0xD7E3BC"},
      			{label:"Blue 1", color:"0xB8CCE4"},
      			{label:"Red 1", color:"0xF2DCDB"},
      			
      			{label:"Grey 1", color:"0xddd9c3"},
      			{label:"Green 2", color:"0xC3D69B"},
      			{label:"Blue 2", color:"0x92CDDC"},
      			{label:"Red 2", color:"0xF2A3A7"},
      			
      			
      			{label:"Grey 2", color:"0x7f7f7f"},
      			{label:"Green 3", color:"0x9BBB59"},
      			{label:"Blue 3", color:"0x31859B"},
      			{label:"Red 3", color:"0xEB757B"},
      			
      			{label:"Grey 3", color:"0x393939"},
      			{label:"Green 4", color:"0x274220"},
      			{label:"Blue 4", color:"0x1F497D"},
      			{label:"Red 4", color:"0x9F2936"},
                ]
                
      public function CustomColorPicker ()
      {
         super();
         this.dataProvider = niceColors;
         this.editable = false;
         this.setStyle("swatchPanelStyleName", "colorSwatchPanel");   
      }
       
   }
}