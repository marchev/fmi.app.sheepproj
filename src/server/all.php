<?php

// strip slashes, if needed
if(get_magic_quotes_gpc()) {
	$_REQUEST = array_map('stripslashes', $_REQUEST);
}
// prevent sql injection
$_REQUEST = array_map('mysql_real_escape_string', $_REQUEST); 

include_once('config.php');
include_once('funcs.php');

?>
