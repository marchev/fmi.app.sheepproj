CREATE DATABASE `sheepproj` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;

create table users (
  id   int unsigned not null primary key auto_increment,
  user    varchar(256) not null,
  pass     char(32) not null,
  
  regtime    datetime not null,
  lastlogtime datetime not null
);

ALTER TABLE users ADD UNIQUE (user);

create table sheeps (
  id   int unsigned not null primary key auto_increment,
  user_id int unsigned not null,
  ordr    int unsigned not null,
  
  name    varchar(256) not null,
  description   text not null,
  color   int unsigned not null,
  priority int not null,
  progress int not null,
  
  completedtime datetime,
  isremoved char not null
);

insert into users values (
  NULL, "demo@sheeptask.com", md5("demo"), NOW(), NOW()
);

insert into sheeps values
  (NULL, (select max(users.id) from users), 1, "Demo task 1", "The description of a demo task 1.", 0, 2, 30, NULL, 'n'),
  (NULL, (select max(users.id) from users), 2, "Demo task 2", "The description of a demo task 2.", 0, 2, 40, NULL, 'n'),
  (NULL, (select max(users.id) from users), 3, "Demo task 3", "The description of a demo task 3.", 0, 2, 50, NULL, 'n'),
  (NULL, (select max(users.id) from users), 4, "Demo task 1 (completed)", "The description of a demo task 4.", 0, 2, 65, NULL, 'y'),
  (NULL, (select max(users.id) from users), 5, "Demo task 2 (completed)", "The description of a demo task 5.", 0, 2, 79, NULL, 'y');




