<?php

include_once('all.php');

if (!($db = db_connect())) {
	message_die('Failed to connect to DB.', __LINE__, __FILE__);
}

// assign the data passed from Flex to variables
$username = $_REQUEST["username"];
$password = $_REQUEST["password"];

if (empty($username)) {
	$output = convert_to_xml(array('login' => 'failed', 'reason' => 'No username given.'));
} else if (empty($password)) {
	$output = convert_to_xml(array('login' => 'failed', 'reason' => 'No password given.'));
} else {
	// query the database to see if the given username/password combination is valid.
	$user = db_get_user($username, $password);

	// return the answer if such user exists
	if (!$user) {
		$output = convert_to_xml(array('login' => 'failed', 'reason' => 'Wrong user or password.'));
	} else {
		$output = convert_to_xml(array('login' => 'ok'));
	}
}

//output all the XML
print($output);

?>