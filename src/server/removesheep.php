<?php

include_once('all.php');

if (!($db = db_connect())) {
	message_die('Failed to connect to DB.', __LINE__, __FILE__);
}

// assign the data passed from Flex to variables
$username = $_REQUEST["username"];
$password = $_REQUEST["password"];
$id = intval($_REQUEST["id"]);


if (empty($username)) {
	$output = convert_to_xml(array('request' => 'failed', 'reason' => 'No username given.'));
} else if (empty($password)) {
	$output = convert_to_xml(array('request' => 'failed', 'reason' => 'No password given.'));
} else {
	// query the database to see if the given username/password combination is valid.
	$user = db_get_user($username, $password);
	if (!$user) {
		$output = convert_to_xml(array('request' => 'failed', 'reason' => 'Wrong user or password.'));
	} else {
		// update sheep with a given id
		$r = db_update_sheep_remove($id, $user["id"]);
		if (!$r) {
			$output = convert_to_xml(array('request' => 'failed', 'reason' => 'Failed to update record.'));
		} else {
			$output = convert_to_xml(array('request' => 'ok'));
		}
	}
}

//output all the XML
print($output);

?>