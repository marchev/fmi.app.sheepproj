<?php

include_once('all.php');

if (!($db = db_connect())) {
	message_die('Failed to connect to DB.', __LINE__, __FILE__);
}

// assign the data passed from Flex to variables
$username = $_REQUEST["username"];
$password = $_REQUEST["password"];
$query = $_REQUEST["query"];

if (empty($username)) {
	$output = convert_to_xml(array('request' => 'failed', 'reason' => 'No username given.'));
} else if (empty($password)) {
	$output = convert_to_xml(array('request' => 'failed', 'reason' => 'No password given.'));
} else if (empty($query)) {
	$output = convert_to_xml(array('request' => 'failed', 'reason' => 'Empty query.'));
} else {
	// query the database to see if the given username/password combination is valid.
	$user = db_get_user($username, $password);
	if (!$user) {
		$output = convert_to_xml(array('request' => 'failed', 'reason' => 'Wrong user or password.'));
	} else {
		if ($query == "ALL") {
			$output = convert_to_xml(array('request' => 'ok', 'data' => array('item' => db_get_all_sheeps($user['id']))));
		} else if ($query == "ALL_ACTIVE") {
			$output = convert_to_xml(array('request' => 'ok', 'data' => array('item' => db_get_all_active_sheeps($user['id']))));
		} else if ($query == "ALL_REMOVED") {
			$output = convert_to_xml(array('request' => 'ok', 'data' => array('item' => db_get_all_removed_sheeps($user['id']))));
		} else {
			$output = convert_to_xml(array('request' => 'failed', 'reason' => 'Not supported query "'. $query .'".'));
		}
	}
}

//output all the XML
print($output);

?>