<?php

// connect to database and select the database
function db_connect() {
	$result = mysql_pconnect(DATABASE_SERVER, DATABASE_USER, DATABASE_PASS);
	if (!$result) {
		return false;
	}
	if (!mysql_select_db(DATABASE_NAME)) {
		return false;
	}

	return $result;
}

// converts the db result to array
function db_result_to_assoc_array($result) {
	$res_array = array();

	for ($count = 0; $row = @mysql_fetch_assoc($result); $count++) {
		$res_array[$count] = $row;
	}

	return $res_array;
}

// retrieves a user by name & pass
function db_get_user($user, $pass) {
	$q = "select * from users where user='$user' and pass='". md5($pass) ."'";
	if (!($r = mysql_query($q))) {
		return false;
	}
	return mysql_fetch_array($r);
}

// create a user
// returns:
// - true if user is created
// - alse it creation failed
function db_create_user($user, $pass) {
	$q = "insert into users values 
		  (NULL,
		   '". $user ."',
		   '". md5($pass) ."',
		   now(),
		   now()
		   )";
		   
	if (!($r = mysql_query($q))) {
		return false;
	}

	return true;
}

// returns all sheeps of a given user, that are not removed from the 'active sheeps' view
// ok
function db_get_all_active_sheeps($user_id) {
	$q = "select * from sheeps 
		where user_id='$user_id' and isremoved='n' 
		order by ordr";
	if (!($r = mysql_query($q))) {
		return false;
	}
	//print_r(mysql_fetch_array($r));
	return db_result_to_assoc_array($r);
}

// returns all already removed from the main view completed sheeps
// ok
function db_get_all_removed_sheeps($user_id) {
	$q = "select * from sheeps 
		where user_id='$user_id' and isremoved='y' 
		order by ordr";
	if (!($r = mysql_query($q))) {
		return false;
	}
	return db_result_to_assoc_array($r);
}

// returns all sheeps
function db_get_all_sheeps($user_id) {
	$q = "select * from sheeps 
		where user_id='$user_id' 
		order by ordr";
	if (!($r = mysql_query($q))) {
		return false;
	}
	return db_result_to_assoc_array($r);
}

// returns assoc array of a given sheep
// ok
function db_get_sheep($id, $user_id) {
	$q = "SELECT * FROM sheeps WHERE id=$id AND user_id=$user_id";
	if (!($r = mysql_query($q))) {
		return false;
	}
	return mysql_fetch_assoc($r);
}

// updates a sheep; if operation was successful, it returns true, else - false.
function db_update_sheep($id, $user_id, $ordr, $name, $description, $color, $priority, $progress, $isremoved) {
	//print "$id, $user_id, $name, $description, <$color>, $priority, $progress";
	$q = "SELECT progress FROM sheeps WHERE id=$id AND user_id=$user_id";
	//print "<br>$q<br>";
	if (!($r = mysql_query($q))) {
		return false;
	}
	$oldProgress = mysql_result($r, 0, 0);
	if ($oldProgress != 100 && $progress == 100) {
		$completedtime = "NOW()";
	} else if ($progress == 100) {
		$completedtime = "completedtime";	// it does not change
	} else {
		$completedtime = "NULL";
	}

	//$completedtime = "NULL";

	$q = "UPDATE sheeps SET
			ordr=\"$ordr\",
			name=\"$name\",
			description=\"$description\",
			color=$color,
			priority=$priority,
			progress=$progress, 
			completedtime=$completedtime,
			isremoved=\"$isremoved\"
			WHERE id=$id AND user_id=$user_id";
	//print "<br>$q<br>";
	
	// append_to_file("q = \n$q\n===============================\n");
	
	if (!($r = mysql_query($q))) {
		return false;
	}
	return true;
}

// removes the sheep from the view of active sheeps
// ok
function db_update_sheep_remove($id, $user_id) {
	// the task should be completed before it can be removed -- no, it won't. trust the not already saved in the DB data!
	/*$q = "SELECT progress FROM sheeps WHERE id=$id AND user_id=$user_id";
	if (!($r = mysql_query($q))) {
		return false;
	}
	append_to_file(mysql_result($r, 0, 0));
	if (mysql_result($r, 0, 0) != '100') {
		return false;
	}*/

	$q = "UPDATE sheeps SET 
			isremoved='y' 
			WHERE id=$id AND user_id=$user_id";
	if (!($r = mysql_query($q))) {
		return false;
	}
	return true;
}

// create a sheep and return it
function db_create_sheep($user_id, $name, $description, $color, $priority, $progress) {
	$q = "insert into sheeps values 
		(NULL, $user_id, 1,
		\"$name\", \"$description\", 
		$color, $priority, $progress, 
		NULL, 'n'
		)";
	if (!($r = mysql_query($q))) {
		return false;
	}
	
	// get max id so far
	$q = "select max(id) from sheeps";
	if (!($r = mysql_query($q))) {
		return false;
	}
	$maxId = mysql_result($r, 0, 0);
	
	$q = "update sheeps set ordr=$maxId where id=$maxId";
	if (!($r = mysql_query($q))) {
		return false;
	}
	
	return db_get_sheep($maxId, $user_id);
}

// delete permanently a given sheep
// TODO: add check if it really deleted something
function db_delete_sheep($id, $user_id) {
	$q = "delete from sheeps where id='$id' and user_id='$user_id'";
	if (!($r = mysql_query($q))) {
		return false;
	}
	return true;
}


// exits, showing a message and the line and file, where the error occured
function message_die($err_msg, $line='undefined', $file='undefined') {
	echo '<b>Error at line '.$line.' in file '.$file.'<br>'.$err_msg.'</b>';
	exit;
}

function append_to_file($s) {
	if (!$handle = fopen('mydebugfile.txt', 'a')) {
		 echo "Cannot open file";
		 exit;
	}
	if (fwrite($handle, $s) === FALSE) {
		echo "Cannot write to file";
		exit;
	}
	fclose($handle);
}


// The main function for converting to an XML document.
// Pass in a multi dimensional array and this recrusively loops through and builds up an XML document.
// 
// @param array $data
// @param string $rootNodeName - what you want the root node to be - defaults to item.
// @param SimpleXMLElement $xml - should only be used recursively
// @return string XML
function convert_to_xml($data, $rootNodeName='item', &$xml=null) {
	// turn off compatibility mode as simple xml throws a wobbly if you don't.
	if (ini_get('zend.ze1_compatibility_mode') == 1) {
		ini_set('zend.ze1_compatibility_mode', 0);
	}

	if (is_null($xml)) {
		$xml = simplexml_load_string("<root></root>");
	}

	// loop through the data passed in.
	foreach ($data as $key => $value) {
		// if numeric key, assume array of rootNodeName elements
		if (is_numeric($key)) {
			$key = $rootNodeName;
		}

		// delete any char not allowed in XML element names
		$key = preg_replace('/[^a-z0-9\-\_\.\:]/i', '', $key);

		// if there is another array found recrusively call this function
		if (is_array($value)) {
			// create a new node unless this is an array of elements
			$node = is_assoc($value) ? $xml->addChild($key) : $xml;

			// recrusive call - pass $key as the new rootNodeName
			convert_to_xml($value, $key, $node);
		} else {
			// add single node.
			$value = htmlentities($value);
			$xml->addChild($key, $value);
		}
	}
	// pass back as string. or simple xml object if you want
	return $xml->asXML();
}

// determine if a variable is an associative array
function is_assoc($array) {
	return (is_array($array) && 0 !== count(array_diff_key($array, array_keys(array_keys($array)))));
}

?>