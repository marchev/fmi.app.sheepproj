<?php

include_once('all.php');

if (!($db = db_connect())) {
	message_die('Failed to connect to DB.', __LINE__, __FILE__);
}

// assign the data passed from Flex to variables
$username = $_REQUEST["username"];
$password = $_REQUEST["password"];
$password2 = $_REQUEST["password2"];

if (empty($username)) {
	$output = convert_to_xml(array('register' => 'failed', 'reason' => 'No username given.'));
} else if (empty($password) || empty($password2)) {
	$output = convert_to_xml(array('register' => 'failed', 'reason' => 'Not both passwords given.'));
} else if ($password != $password2) {
	$output = convert_to_xml(array('register' => 'failed', 'reason' => 'The passwords are not the same.'));
} else {
	// query the database to see if the given username/password combination is valid.
	$res = db_create_user($username, $password);

	if (!$res) {
		$output = convert_to_xml(array('register' => 'failed', 'reason' => 'The user already exists. Please choose another name.'));
	} else {
		$output = convert_to_xml(array('register' => 'ok'));
	}
}

//output all the XML
print($output);

?>