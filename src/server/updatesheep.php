<?php

include_once('all.php');

if (!($db = db_connect())) {
	message_die('Failed to connect to DB.', __LINE__, __FILE__);
}

// assign the data passed from Flex to variables
$username = $_REQUEST["username"];
$password = $_REQUEST["password"];
$id = intval($_REQUEST["id"]);
$ordr = intval($_REQUEST["ordr"]);
$name = $_REQUEST["name"];
$description = $_REQUEST["description"];
$color = intval($_REQUEST["color"]);
$priority = intval($_REQUEST["priority"]);
$progress = intval($_REQUEST["progress"]);
$isremoved = $_REQUEST["isremoved"];


if (empty($username)) {
	$output = convert_to_xml(array('request' => 'failed', 'reason' => 'No username given.'));
} else if (empty($password)) {
	$output = convert_to_xml(array('request' => 'failed', 'reason' => 'No password given.'));
} else if ($username == DEMO_USER) {
	$output = convert_to_xml(array('request' => 'failed', 'reason' => 'Demo user cannot handle information on the server.'));
} else if ($priority != 0 && $priority != 1 && $priority != 2) {
	$output = convert_to_xml(array('request' => 'failed', 'reason' => 'Invalid priority.'));
} else if ($progress < 0 || $progress > 100) {
	$output = convert_to_xml(array('request' => 'failed', 'reason' => 'Invalid progress.'));
} else {
	// query the database to see if the given username/password combination is valid.
	$user = db_get_user($username, $password);
	if (!$user) {
		$output = convert_to_xml(array('request' => 'failed', 'reason' => 'Wrong user or password.'));
	} else {
		// update sheep with a given id
		$r = db_update_sheep($id, $user["id"], $ordr, $name, $description, $color, $priority, $progress, $isremoved);
		if ($r === false) {
			$output = convert_to_xml(array('request' => 'failed', 'reason' => 'Failed to update record.'));
		} else {
			$output = convert_to_xml(array('request' => 'ok', 'data' => array('item' => db_get_sheep($id, $user['id']))));
		}
	}
}

//output all the XML
print($output);

?>