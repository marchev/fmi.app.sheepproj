# Sheep Organizer: An Adobe Flex Kanban TODO List #

Sheep Organizer is a Kanban TODO list, implemented as an Adobe Flex app. It features also a PHP web server for persistence of the TODO items. This is [Svilen Marchev](https://plus.google.com/u/0/+SvilenMarchev)'s and Tsvetelina Raykova's course project for the Adobe Flex class, led in 2010 at the Faculty of Mathematics and Informatics at Sofia University. Although it is fully functional, 80% of the code was written during the last 15 hours before the exam, so code quality expectations should not be set overwhelmingly high. :)

## Structure ##

### Flex app ###

* Offers two modes of operation - demo mode, requiring no registration but not allowing persistence to server, and signed in mode.
* Users can create a task (aka a "sheep"), assigning it a name, description, priority, and color.
* Users can slide a task to the right, denoting its progress.
* As soon as a task reaches the right most point, it's considered completed and the app suggests moving it to the "history".
* Users can see a list of all completed tasks.
* Users can save the entire tasks space to the server.
* Users can restore their tasks context (aka "herd" :-)) from the server.

### PHP back-end ###

* Very light PHP web server, exposing an API for creating, updating, deleting, and listing tasks.
* Uses MySQL for persistence.
* Doesn't offer any UI.

### UI states ###

![state-flow.png](https://bitbucket.org/repo/G4y5g7/images/1180276138-state-flow.png)

![main-with-clarification.png](https://bitbucket.org/repo/G4y5g7/images/1666472241-main-with-clarification.png)

## Screenshots ##

![login.png](https://bitbucket.org/repo/G4y5g7/images/4279133800-login.png)

![register.png](https://bitbucket.org/repo/G4y5g7/images/3683094873-register.png)

![main-with-tasks2.png](https://bitbucket.org/repo/G4y5g7/images/4149899007-main-with-tasks2.png)

![main-with-history.png](https://bitbucket.org/repo/G4y5g7/images/3409028458-main-with-history.png)

### Setup ###

The bin/ dir contains a binary that doesn't require a web server and thus you might try out immediately:

* Download [the zip](https://bitbucket.org/marchev/fmi.app.sheepproj/raw/master/bin/SheepProject-debug.zip) and extract it.
* Open main.html.
* Choose "demo mode" at app startup, since it doesn't require a web server.
* Laughing is good for your health! :D

If you want to leverage the persistence layer:

* Run the PHP server.
* Create the necessary MySQL tables (a SQL script is included in the sources).
* Import the Flex app into your Flex IDE.
* Configure the Flex app with the correct server address.
* Compile & run.